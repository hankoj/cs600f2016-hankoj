\contentsline {chapter}{List of Figures}{viii}
\contentsline {chapter}{\numberline {1}Introduction}{1}
\contentsline {section}{\numberline {1.1}Motivation}{2}
\contentsline {section}{\numberline {1.2}Current State of the Campus Map}{3}
\contentsline {section}{\numberline {1.3}Goals of the Project}{6}
\contentsline {section}{\numberline {1.4}Thesis Outline}{7}
\contentsline {chapter}{\numberline {2}Related Work}{8}
\contentsline {section}{\numberline {2.1}Primary Sources}{8}
\contentsline {subsection}{\numberline {2.1.1}University of Wisconsin Map Project}{8}
\contentsline {section}{\numberline {2.2}Similar College Campus Maps}{11}
\contentsline {subsection}{\numberline {2.2.1}NCAC Schools}{11}
\contentsline {subsection}{\numberline {2.2.2}Oregon University}{12}
\contentsline {subsection}{\numberline {2.2.3}University College of London}{13}
\contentsline {subsection}{\numberline {2.2.4}Emory University}{14}
\contentsline {chapter}{\numberline {3}Campus Map Design}{16}
\contentsline {section}{\numberline {3.1}Map Components}{16}
\contentsline {subsection}{\numberline {3.1.1}Visual Components}{16}
\contentsline {subsection}{\numberline {3.1.2}Technical Components}{17}
\contentsline {subsection}{\numberline {3.1.3}Base Map}{19}
\contentsline {subsection}{\numberline {3.1.4}Data Layers}{19}
\contentsline {subsection}{\numberline {3.1.5}Map Data}{20}
\contentsline {section}{\numberline {3.2}Desired Functionality}{20}
\contentsline {chapter}{\numberline {4}Campus Map Implementation}{21}
\contentsline {subsection}{\numberline {4.0.1}License Preface}{21}
\contentsline {section}{\numberline {4.1}Overview of Campus Map}{21}
\contentsline {section}{\numberline {4.2}Technical Components of the Campus Map}{23}
\contentsline {section}{\numberline {4.3}Map Outlines}{23}
\contentsline {subsection}{\numberline {4.3.1}Allegheny College}{23}
\contentsline {subsection}{\numberline {4.3.2}Robertson Athletic Complex}{24}
\contentsline {section}{\numberline {4.4}Labels}{25}
\contentsline {section}{\numberline {4.5}Sidebar}{26}
\contentsline {section}{\numberline {4.6}Search Bar}{28}
\contentsline {section}{\numberline {4.7}Markers and Popups}{29}
\contentsline {section}{\numberline {4.8}Map Legend}{31}
\contentsline {chapter}{\numberline {5}Discussion and Future Work}{34}
\contentsline {section}{\numberline {5.1}Map Usage}{34}
\contentsline {section}{\numberline {5.2}Discussion on the Two Map APIs}{35}
\contentsline {section}{\numberline {5.3}Project Limitations}{36}
\contentsline {section}{\numberline {5.4}Future Work}{36}
\contentsline {section}{\numberline {5.5}Conclusion}{37}
\contentsline {chapter}{\numberline {A}HTML Code}{38}
\contentsline {chapter}{\numberline {B}JavaScript Code}{40}
\contentsline {chapter}{\numberline {C}CSS Code}{56}
\contentsline {chapter}{Bibliography}{58}
