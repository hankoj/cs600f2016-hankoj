%
% $Id: ch01_overview
%
%   *******************************************************************
%   * SEE THE MAIN FILE "AllegThesis.tex" FOR MORE INFORMATION.       *
%   *******************************************************************

\chapter{Introduction}\label{ch:intro} % we can refer to chapter by the label

%   ************************************************************************
%   * In LaTeX, new paragraphs are begun by simply leaving a blank line in *
%   * the LaTeX file.                                                      *
%   *                                                                      *
%   * The \\ characters should NEVER be used to end a paragraph.           *
%   * They are used only for inserting line breaks in certain situations.  *
%   *                                                                      *
%   * "Widows" (ending paragraph lines at the top of a new page) and       *
%   * "orphans" (opening paragraph lines at the bottom of a page) should   *
%   * be eliminated; this sometimes requires re-writing some of the        *
%   * text to change the line lengths.                                     *
%   ************************************************************************

\textit{Where is the Campus Center?}  \textit{Where is this Walker Hall?}  \textit{What is the best route I would take from the gym to the dining hall?}  These are all questions that new students, prospective students, and parents all ask themselves--or each other--when first arriving to a college campus.  If you're a prospective student, the first step to finding a college to attend would be to visit their website and see what the school has to offer.  One of the most obvious things to look at is the campus, which is laid out on the campus map.  An old-fashioned, hand-drawn map in this day and age could draw a prospective student away from a given school.  As a new student, studying and memorizing the campus map is a first week of school ritual, ensuring that you know exactly where to go and how to get there.  As a visitor, whether that be a new professor, a parent or a relative of a student, the campus map is your best resource for finding the dining hall, library, or a specific residence hall.  It is not an unbelievable coincidence that the campus map is crucial for upholding an institution's rank and reputation.  With today's technology, campus maps have the potential to be so much more than a hand-drawn lay out displaying a number over each building, which is what six of the ten North Coast Athletic Conference (NCAC) colleges currently have on their respective websites, including Allegheny College \cite{alleghenymap, kenyonmap, woostermap, denisonmap, depauwmap, wittmap}.

Maps have been a crucial instrument for as long as humanity has been around.  One crucial map-based applicaton, OpenStreetMap, started up at the University College of London in 2004 \cite{haklay2008openstreetmap}.  OpenStreetMap (OSM) had a vision of becoming the Wikipedia of mapping systems by following the peer production model, more commonly known as crowdsourcing \cite{haklay2008openstreetmap}.  OSM started to take off when groups of friends planned mapping parties, gathering in a specfic bounded area where they would wander and collect GPS data to upload to the database \cite{haklay2008openstreetmap}.  As of 2013, OSM has over one million registered users \cite{osmblog}.  In 2005 \cite{svennerberg2010beginning}, Google Maps was born, adding to the revolutionary changes to navigation.  Google Maps provides navigation of Geographic Information Systems (GIS) in a multi-layer fashion, limiting information overloading for the users \cite{liang2012building}.  Users of Google Maps can enjoy a seemingly endless amount of geographic information including directions, sights to see, terrain, art exhibits, and plenty more \cite{svennerberg2010beginning}.  Google Maps API is incredibly powerful for map-building and comes with an extensive documentation to build anything from a simple park map to a multi-building corporate campus \cite{googledev}.  With the creation of these two different map systems, mapping anything from a forest to a city became exponentially easier and more powerful than anything that came before.

Allegheny College is in dire need of an upgrade to the campus map found online.  It is out-dated, hard to use, and not caught up technologically speaking.  This project tackles that task by creating an online campus map for the campus of Allegheny College.


\section{Motivation} \label{sec:motivation}

The motivation for this project stems mainly from personal experiences being a student at Allegheny College.  With an abundance of stories to choose from, whether it be from students, faculty members, or visitors, it is easy to convey the need for an upgraded campus map system.  One such example involves my grandfather visiting campus on move in day to see where I lived my freshman year.  Upon arriving to campus, he had no idea where any of the buildings were so he was forced to use the current campus map to navigate campus.  He ended up driving on several campus roads that cars do not drive on, thus sparking the idea in my head for the need to upgrade our campus map.  Stories like this are not hard to find at Allegheny, which created the perfect motivation to create a system that will help everyone navigate our campus better.

Additionally, many pieces of technology are included in a project such as this one, which make it challenging yet rewarding to complete.  Creating a campus map makes usage of a database, front-end code, back-end code, aesthetics design, and much more.  The outcome of a project such as this one is a usable campus map for all students, faculty, and visitors to use, but the real takeaway is the real-world experience received from tackling such a large problem. 

%   *******************************************************************
%   * FIGURES ARE PLACED ACCORDING TO A SET OF CONSTRAINTS THAT CAN   *
%   * BE MANIPULATED TO SOME DEGREE.                                  *
%   * A SEARCH FOR "controlling latex floats" TURNS UP A NUMBER OF    *
%   * SITES THAT HAVE USEFUL INFORMATION, FOR EXAMPLE:                *
%   *                                                                 *
%   * http://mintaka.sdsu.edu/GF/bibliog/latex/floats.html            *
%   * http://goo.gl/aC8E8Q                                            *
%   * http://robjhyndman.com/hyndsight/latex-floats/                  *
%   *******************************************************************


\section{Current State of the Campus Map}\label{sec:stateofart}

Shown below in Figure 1.1 is the current campus map found on the Allegheny College website.  There are many things worth mentioning that can be improved on this map.  The most obvious improvement that can be made just by looking at this map is that it is a PDF document.  As previously mentioned in the introduction, we are in a technological era dominated by data and information at our fingertips.  This current campus map fails to comply with this trend.  In order to view this campus map, you either have to download and print it out and always have it on hand, or save it to your mobile device and spend time zooming in and out in different locations to attempt to navigate campus.  Another enhancement opportunity is color coded labels for each academic building, faculty building, student dormitory, and parking lot.  In the current map, you can see that there is no color coordination, just the colors you would see if you saw the building.  When you need to find a building or road when looking at a campus, it makes sense that there is the ability to search the area.  The map in PDF form has no way to search the contents, other than scanning it over and over with your eyes until the desired location is found.  Also, in this current state, the map is not able to be updated without redrawing and reprinting the entire map.  With an online campus map, the features mentioned above can be both improved and enhanced.  Any future changes, updates, upgrades, or enhancements could all be made easily and much quicker if the map was hosted online.

\begin{figure}[!h]
\includegraphics[scale=.23]{images/alleghenycampusmap.jpg}
\centering
\caption{Current Allegheny College Campus Map found at \protect \url{Allegheny.edu} \cite{alleghenymap}}
\end{figure}

\newpage

In addition to the current state of Allegheny College's campus map, it's worth mentioning the current state of Allegheny College on Google Maps.  Shown below in Figure 1.2, it is easy to see that not much of campus is labeled on Google Maps native map.  A few buildings, such as Caflisch Hall and Baldwin Hall, are labeled, but many buildings are missing.  In addition to content missing, there is information not needed such as the Semi Truck repair label located at the bottom of the image.  This further proves the need that the two current map options for Allegheny College need improvements.  In the current states of both maps, Allegheny College is failing to provide students with the most amount of information about campus in the most efficient way.

\begin{figure}[!h]
\includegraphics[scale=.45]{images/alleghenygooglemaps.png}
\centering
\caption{Current Allegheny College Campus Map found on Google Maps \cite{googlemaps}}
\end{figure}

\newpage

\section{Goals of the Project}\label{sec:goals}

This project aims to upgrade our current, hand-drawn Allegheny College campus map to an online campus map.  The goals of this project are to improve campus navigation, draw in more prospective students using an up-to-date campus map in terms of technology, and to be one step ahead of our competitors.  With an upgrade from a hand-drawn, outdated campus map, visitors of the new, online campus map will think of our school as more technologically advanced than other schools with the mentioned dated maps.  Hand-drawn maps have become outdated for several reasons.  First of all, they are difficult to read and understand, especially when on a time schedule, much like a new student on the first day of classes.  Secondly, they make the institution old-fashioned, and not in a traditional way.  Colleges, among other institutions, need to show potential students and current students that they have the ability to adapt and keep up with changing times.  Failing to do so could result in a lowered enrollment and decrease in national ranks, if over enough time the college stays too old-fashioned.  Students in college in this digital era are used to having a plethora of information at their very fingertips.  A lost student or campus visitor has to currently open up our campus map's PDF file, after searching for it online, and navigate it using the current system of simply scanning it until the needed information is found.  This, one would think, is not the optimal way to acquire this information.  A campus map that is hosted online and based off of one of the aforementioned map services can be found, opened, and navigated significantly quicker, matching the speed at which this digital age considers normal.  In addition to ease of access and use, an online hosted map will open up doors for student run organizations or other community groups to ask for modification of the map to include events and other markers on the map.

In addition to benefiting campus, a goal of this project was for me to personally learn more about creating a project of this nature.  In recent summers, I have had some experience with both front-end and back-end software development, as well as some of it being web development.  I by no means would have considered myself adequate in coding in Javascript prior to this project, so learning a new language along with the process of creating a custom campus map sets up a unique experience for me.


% COMMENTED OUT NEXT FEW LINES TO SAVE SPACE; MAY PUT THEM BACK LATER
%Following the concise statement of the thesis, some of the details can be
%expanded.  
%It is appropriate to
%refer to some of the results in the introduction (which may 
%mean going back and adding them to the introduction once the
%research is completed). 
%A senior thesis, or any research paper, is not a mystery 
%novel---there is no need to keep the reader in suspense about what
%has been accomplished.

\section{Thesis Outline}\label{sec:outline}
Chapter two discusses other college and university maps that have online campus maps, including one paper published by the University of Wisconsin, to which I owe a lot of inspiration and source of knowledge on a project of this nature.  Chapter three discusses what goes into the design of a map project in general, and specifically what went into this project.  Chapter four goes over every part of the implementation of the campus map, with source code and screenshots to demonstrate the work.  The paper is summed up in chapter five, with multiple discussions on the usage of the map, the map APIs, limitations, future work, and a conclusion.
