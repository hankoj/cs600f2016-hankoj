%
% $Id: ch04_implementation.tex
%
%   *******************************************************************
%   * SEE THE MAIN FILE "AllegThesis.tex" FOR MORE INFORMATION.       *
%   *******************************************************************
%
\chapter{Campus Map Implementation}\label{ch:implem}

\subsection{License Preface}
To preface this section, it is necessary to state that there are code blocks that have been properly modified and attributed in this project's source code.  When dealing with code segments from the Google Developers page, the work was modified and adapted to my project under the Apache 2.0 License.  As for the individual Github projects, the source code was modified and adapted to my project under their individual respective licenses, the MIT License.  When discussed, any work mentioned in this chapter that came from another project will be properly cited, but more details can be found in the source code.

\section{Overview of Campus Map}
Shown below in Figure \ref{campusMap}, a screenshot of this project's completed campus map is shown.

\newpage

\begin{figure}[!h]
\includegraphics[scale=.3]{images/chap4_map_overview.png}
\centering
\caption{Overview of online campus map}
\label{campusMap}
\end{figure}

As you can see from the image, this project's campus map is based off of the OpenStreetMap base map, as noted in the bottom right hand corner of the screen. There are many components that make this map what it is, which will be described in greater detail in the following sections.  As an overview, it is evident that the map contains a boundary of campus, markers on buildings, building labels, a sidebar with a search box on the left hand of the screen, and a legend in the upper right hand corner.  Just at first glance, it is easy to see that this campus map provides more data to the user than our current hand-drawn map.  The components of the map will be discussed in further detail in the subsequent sections.

\section{Technical Components of the Campus Map}

\section{Map Outlines}

\subsection{Allegheny College}
The bounds for Allegheny College can be seen if one visited Google Maps, showing a slightly darker shade of tan than the rest of the map.  As for OpenStreetMaps, the bounds of campus were not clear to the user.  This project had to manually retrieve and plot the latitude and longitude bounds for the polygon that surrounds campus, shown below in Figure \ref{campusOutline}.  Outlining the campus bounds was important to the nature of the project, due to some buildings and areas of campus not being a visibly obvious part of campus.

\begin{figure}[!h]
\includegraphics[scale=.45]{images/chap4_outline_ac.png}
\centering
\caption{Allegheny College outline}
\label{campusOutline}
\end{figure}

\subsection{Robertson Athletic Complex}
Similar circumstances arose for the Robertson Athletic Complex, Allegheny's home to outdoor sports slightly off campus to the North.  The bounds for this complex too had to manually collected.  The bounds of the sports complex can be clearly seen below in Figure \ref{robertsonOutline}.  It was also important to make a separate outline of this complex due to the fact that it is off campus and could be easily mistaken as not part of campus to a new set of eyes.

\begin{figure}[!h]
\includegraphics[scale=.5]{images/chap4_outline_r.png}
\centering
\caption{Robertson Athletic Complex outline}
\label{robertsonOutline}
\end{figure}

The polygon drawn for the Robertson Athletic Complex, as well as the one for Allegheny College, was created using a Leaflet function to add geoJSON data to the map, shown below in Figure \ref{outlineCode}.  To create the two JSON objects, a type, property name, and polygon coordinates were given to the variable.  Using the geoJSON method in Leaflet, this project was able to draw the polygons and add them to the map.  Due to these outlines needing to be visable at all times, the data was not added to a new layer, but rather to the base map itself.

\begin{figure}[!h]
\centering
\lstinputlisting[firstline=174,lastline=199]{Script2.js}
\caption{{\tt Script2.js}: Leaflet method for displaying outlines}
\label{outlineCode}
\end{figure}

\section{Labels}
One important feature that needed to be recreated when producing an online campus map is the labeling of buildings and other places on campus.  OpenStreetMap's base map had the data this project needed, in containing almost all of the desire campus labels.  In Figure \ref{labels}, you can clearly the see the labels for several entities on campus.  The label for our main campus road, North Main St., can be clearly seen.  There are also labels for three dormitories, two academic buildings, our admissions building, and even a parking lot.  Unfortunately, there were a few missing building labels from the base map, discussed in further detail in Chapter 6.

\begin{figure}[!h]
\includegraphics[scale=.5]{images/chap4_labels.png}
\centering
\caption{Screenshot displaying the various labels built into the map}
\label{labels}
\end{figure}

\section{Sidebar}
The sidebar for this map was created using a library hosted on Github called the \texttt{Leaflet-Sidebar} \cite{turbo87}.  The sidebar was a custom control created for the sole purpose to provide a floating sidebar container on either the right or left hand side of the map.  The code below in Figure \ref{sidebarCode} shows how the custom sidebar for this project was created using the Github library.  Again, notice how another layer was not produced for this component.  The sidebar floats above the map, but falls under the category of a map control.  In order to ensure that the user always has access to this designed control, this project opted to leave the sidebar permanently visible to users.  It is also quite common for there to be sidebars containing buttons and data in map projects, as the ones shown in the Related Works section.

\newpage

\begin{figure}[!h]
\centering
\lstinputlisting[firstline=121,lastline=130]{Script2.js}
\caption{{\tt Script2.js}: Code for left hand sidebar \cite{turbo87}}
\label{sidebarCode}
\end{figure}

The sidebar was implemented in this map for the use of a container to hold the Allegheny College logo, search bar and search results, and for the opportunity to display data and other buttons in the future.  Shown below in Figure \ref{sidebarImg}, the sidebar displays the search results for the query ``Alden Hall'', where the computer science department is located.  It is also evident from Figure \ref{sidebarImg} that the sidebar is in fact a floating container over the map, as mentioned previously in this section.

\begin{figure}[!h]
\includegraphics[scale=.65]{images/chap4_sidebar.png}
\centering
\caption{Screenshot displaying the usage of the sidebar}
\label{sidebarImg}
\end{figure}

\section{Search Bar}
The search bar was implemented for this project's map in order to allow users to search for buildings on campus in a quick and simple fashion.  The search bar queries Nominatim \cite{nominatim}, a search tool that allows for reverse geocoding, a method mentioned in a previous chapter.  The code for the Search Bar was implemented in two separate functions, with the help of a Github library called \texttt{Osm-Tools} \cite{deickr}.  The first function responsible for calling the Nominatim tool is shown below in Figure \ref{nominatim}.

\begin{figure}[!h]
\centering
\lstinputlisting[firstline=344,lastline=368]{Script2.js}
\caption{{\tt Script2.js}: Code for Search Bar Query \cite{deickr}}
\label{nominatim}
\end{figure}

The function works by first obtaining the input from the input bar on the web page.  JQuery is then used to call the Nominatim service, adding the input text from the input bar as well as ``Meadville'' and ``PA'' to ensure the responses are only for buildings on Allegheny College's campus.  Everything that comes back from the service call gets formatted in order to output clickable links on the HTML side.  The other function associated with the search bar is simply in charge of drawing a red box if the address is a building, or a green circle if the address returns a node variable.  An example of the search bar in action is shown below in Figure \ref{searchBar}.  The query ``Alden Hall'' is typed into the search bar, again where the computer science department is located, and a red box surrounds the building appears on the map.

\begin{figure}[!h]
\includegraphics[scale=.3]{images/chap4_searchbar_1.png}
\centering
\caption{Screenshot displaying the usage of the search bar}
\label{searchBar}
\end{figure}

\section{Markers and Popups}
Markers and popups were important to this project because it assists users of the map to locate groups of buildings, such as the dormitories or the athletic complexes, much quicker than when using the current campus map.  Using popups bound to the markers as a source of more information only added to the importance of the implementation of this feature.  The markers and popups were created using the Leaflet library, with the additional help of a Github library called \texttt{Leaflet-Color-Markers} that provided a wider variety of colors for the marker, allowing for the differentiation of the building categories \cite{pointhi}.  The code segment shown below in Figure \ref{markerCode} shows how this project made use of arrays and the Leaflet marker object to create different colored markers with popups that display the name of the building for a placeholder.  This specific code segment shows the code for the Academic building makers displayed on the map.

\begin{figure}[!h]
\centering
\lstinputlisting[firstline=223,lastline=228]{Script2.js}
\caption{{\tt Script2.js}: Code for Academic Building Markers and Popups}
\label{markerCode}
\end{figure}

As you can see from the code segment, using an array populated with a list of building names of a few academic buildings found on campus, this project created a new marker, bound a popup to it, and added it to the map.  For demonstration purposes, all markers in this version of the map are on the map layer.  In a future version, the markers will be moved to their own layers, allowing this project to show and hide the markers when prompted by the user.  The image shown below in Figure \ref{markerImg} shows a zoomed in example of the different colored markers found on the map, as well as one of the popups being prompted to show up when clicked.

\newpage

\begin{figure}[!h]
\includegraphics[scale=.4]{images/chap4_markers.png}
\centering
\caption{Screenshot displaying the different markers on the map and the use of a popup}
\label{markerImg}
\end{figure}

\section{Map Legend}
No map is complete without a legend, otherwise the user would have no idea what to make of the different colored markers on the map.  This project made use of the control functionality in Leaflet, similar to the sidebar.  The icons for the legend were also obtained using the \texttt{Leaflet-Color-Markers} library \cite{pointhi} used for the markers, discussed in the previous section.  The code segment shown below in Figure \ref{legendCode} shows how the legend was created.

\newpage

\begin{figure}[!h]
\centering
\lstinputlisting[firstline=285,lastline=312]{Script2.js}
\caption{{\tt Script2.js}: Code for Map Legend}
\label{legendCode}
\end{figure}

Again, the control functionality of Leaflet was used to place a HTML \texttt{div} located in the top right corner of the map.  The legend was added to the map layer, instead of its own, due to the face that legends are an essential part of any map.  Figure \ref{legendImg} below shows what the legend looks like when viewing the map.

\newpage

\begin{figure}[!h]
\includegraphics[scale=1]{images/chap4_legend.png}
\centering
\caption{Screenshot showing the Map Legend}
\label{legendImg}
\end{figure}