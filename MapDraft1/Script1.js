// JavaScript source code
// Jacob Hanko
//Portions of this page are modifications based on work created
//and shared by Google and used according to terms described in the
// Creative Commons 3.0 Attribution License.

var map;

//center coordinates for buildings below
//(obtain from database)
var centerOfAlden = new google.maps.LatLng(41.649282, -80.144806);

//code to hide Google's POIs
var myCustomStyles = [
    {
        featureType: 'poi',
        elementType: 'labels',
        stylers: [{
            visibility: 'off'
        }]
    }];

function initialize() {
    var centerOfCampusLatLng = new google.maps.LatLng(41.649349, -80.1471381);
    var mapOptions = {
        center: centerOfCampusLatLng,
        zoom: 16,
        //mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeId: "OSM",        
        styles: myCustomStyles,
        clickableIcons: true,
        mapTypeControl: true,
        mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
            position: google.maps.ControlPosition.TOP_RIGHT,
            mapTypeIds: ['roadmap', 'satellite']
        }
    };
    

    map = new google.maps.Map(document.getElementById('map'), mapOptions);

     map.mapTypes.set("OSM", new google.maps.ImageMapType({
                getTileUrl: function(coord, zoom) {
                    // See above example if you need smooth wrapping at 180th meridian
                    return "http://tile.openstreetmap.org/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
                },
                tileSize: new google.maps.Size(256, 256),
                name: "OpenStreetMap",
                maxZoom: 19
            }));


    //this function draws the polygon around the robertson complex
    var robertsonComplexCoordinates = [
        { lat: 41.661608, lng: -80.151906 },
        { lat: 41.661520, lng: -80.143312 },
        { lat: 41.654554, lng: -80.146380 },
        { lat: 41.654762, lng: -80.147582 },
        { lat: 41.654249, lng: -80.150371 },
        { lat: 41.660397, lng: -80.152045 },
    ];

    //outline of campus coordinates
    var alleghenyCampusCoordinates = [
        { lat: 41.647078, lng: -80.149186 },
        { lat: 41.648277, lng: -80.148743 },
        { lat: 41.650001, lng: -80.148035 },
        { lat: 41.650061, lng: -80.148046 },
        { lat: 41.651091, lng: -80.147638 },
        { lat: 41.651352, lng: -80.147558 },
        { lat: 41.652096, lng: -80.147223 },
        { lat: 41.652290, lng: -80.146236 },
        { lat: 41.651607, lng: -80.144055 },
        { lat: 41.651599, lng: -80.143942 },
        { lat: 41.651894, lng: -80.142566 },
        { lat: 41.652053, lng: -80.141746 },
        { lat: 41.652001, lng: -80.141714 },
        { lat: 41.651793, lng: -80.141880 },
        { lat: 41.651721, lng: -80.141923 },
        { lat: 41.651581, lng: -80.141950 },
        { lat: 41.651405, lng: -80.141912 },
        { lat: 41.650776, lng: -80.141697 },
        { lat: 41.650766, lng: -80.141588 },
        { lat: 41.648397, lng: -80.140960 },
        { lat: 41.648349, lng: -80.140960 },
        { lat: 41.647724, lng: -80.141357 },
        { lat: 41.647487, lng: -80.140257 },
        { lat: 41.646906, lng: -80.140659 },
        { lat: 41.646854, lng: -80.142376 },
        { lat: 41.646800, lng: -80.142422 },
        { lat: 41.646970, lng: -80.142961 },
        { lat: 41.646569, lng: -80.143149 },
        { lat: 41.646571, lng: -80.143159 },
        { lat: 41.646850, lng: -80.144063 },
        { lat: 41.646303, lng: -80.144355 },
        { lat: 41.647033, lng: -80.146678 },
        { lat: 41.646372, lng: -80.147048 }
    ];

    //alden coordinates
    var aldenHallCoordinates = [
        { lat: 41.649433, lng: -80.144972 },
        { lat: 41.649297, lng: -80.144556 },
        { lat: 41.649146, lng: -80.144639 },
        { lat: 41.649284, lng: -80.145060 }
    ];

    //roberston outline
    var robertsonComplexOutline = new google.maps.Polygon({
        path: robertsonComplexCoordinates,
        geodesic: true,
        strokeColor: '#009900',
        strokeOpactity: 1.0,
        strokeWeight: 1.5,
        fillOpacity: 0.0
    });

    //campus outline
    var alleghenyCampusOutline = new google.maps.Polygon({
        path: alleghenyCampusCoordinates,
        geodesic: true,
        strokeColor: '#102d5b',
        strokeOpactity: 1.0,
        strokeWeight: 1.5,
        fillOpacity: 0.0
    });

    var aldenHallOutline = new google.maps.Polygon({
        path: aldenHallCoordinates,
        geodesic: true,
        strokeColor: '#102d5b',
        strokeOpactity: 1.0,
        strokeWeight: 1,
        fillOpacity: 0.0
    });

    //sets the map to show outlines 
    robertsonComplexOutline.setMap(map);
    alleghenyCampusOutline.setMap(map);
    //aldenHallOutline.setMap(map);

    var aldenContentString = '<div id="content">' +
        '<div id="siteNotice">' +
        '</div>' +
        '<h1 id="firstHeading" class="firstHeading">Alden Hall</h1>' +
        '<div id="bodyContent">' +
        '<p>Named in honor of the college`s founder, Timothy Alden, the present structure was built on the foundation of the first Alden Hall (1905), which was destroyed by fire in January 1915. Today, Alden Hall houses the Geology and Computer Science. </p>' +
        '</div>' +
        '</div>';
    var aldenInfoWindow = new google.maps.InfoWindow({
        content: aldenContentString
    });

    var aldenMarker = new google.maps.Marker({
        position: centerOfAlden,
        title: "Alden Hall"
        //icon: "aldenText.png"
    });

   //alden infobox used to be here
    
    aldenMarker.addListener('click', function () {
        aldenInfoWindow.open(map, aldenMarker);
    });

    //sets markers
    //aldenMarker.setMap(map);

    /* Block of Code for Search Box Functionality */
    /******************************************************************/
    // Create the search box and link it to the UI element.
    var input = document.getElementById('pac-input');

    var autocomplete = new google.maps.places.Autocomplete(input, { placeIdOnly: true });
    autocomplete.bindTo('bounds', map);

    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    var infowindow = new google.maps.InfoWindow();
    var infowindowContent = document.getElementById('infowindow-content');
    infowindow.setContent(infowindowContent);
    var geocoder = new google.maps.Geocoder;
    var marker = new google.maps.Marker({
        map: map
    });
    marker.addListener('click', function () {
        infowindow.open(map, marker);
    });

    autocomplete.addListener('place_changed', function () {
        infowindow.close();
        var place = autocomplete.getPlace();

        if (!place.place_id) {
            return;
        }
        geocoder.geocode({ 'placeId': place.place_id }, function (results, status) {

            if (status !== 'OK') {
                window.alert('Geocoder failed due to: ' + status);
                return;
            }
            map.setZoom(18);
            map.setCenter(results[0].geometry.location);
            // Set the position of the marker using the place ID and location.
            marker.setPlace({
                placeId: place.place_id,
                location: results[0].geometry.location
            });
            marker.setVisible(true);
            infowindowContent.children['place-name'].textContent = place.name;
            infowindowContent.children['place-id'].textContent = place.place_id;
            infowindowContent.children['place-address'].textContent =
                results[0].formatted_address;
            infowindow.open(map, marker);
            //added for obtaining place name for label
            //aldenTextLabel.setContent(place.name.split(',')[0]);
        });
    });

    //code block for gathering place names from place IDs
    var buildingLabelOptions = {
        content: "",
        maxWidth: "150px",
        boxStyle: {
            border: "none",
            textAlign: "center",
            fontSize: "10pt",
            background: "transparent",
            opacity: "0.7",
            padding: "0.5px"
        },
        disableAutoPan: true,
        position: "",
        closeBoxURL: "",
        isHidden: false,
        pane: "mapPane",
        enableEventPropagation: true,
        pixelOffset: new google.maps.Size(-25, 0)
    };
    

var robertsonTextLabel = new InfoBox(buildingLabelOptions);
var doaneTextLabel = new InfoBox(buildingLabelOptions);
var schultzTextLabel = new InfoBox(buildingLabelOptions);
var oddFellowsTextLabel = new InfoBox(buildingLabelOptions);
var nv2TextLabel = new InfoBox(buildingLabelOptions);
var nv1aTextLabel = new InfoBox(buildingLabelOptions);
var nv1bTextLabel = new InfoBox(buildingLabelOptions);
var nv1cTextLabel = new InfoBox(buildingLabelOptions);
var alleghenyCommonsTextLabel = new InfoBox(buildingLabelOptions);
var collegeCourt1TextLabel = new InfoBox(buildingLabelOptions);
var collegeCourt2TextLabel = new InfoBox(buildingLabelOptions);
var collegeCourt3TextLabel = new InfoBox(buildingLabelOptions);
var pkpTextLabel = new InfoBox(buildingLabelOptions);
var ravineTextLabel = new InfoBox(buildingLabelOptions);

//array of text labels
var textLabelArray = new Array(
    robertsonTextLabel,
    doaneTextLabel,
    schultzTextLabel,
    oddFellowsTextLabel,
    nv1aTextLabel,
    nv1bTextLabel,
    nv1cTextLabel,
    alleghenyCommonsTextLabel,
    collegeCourt1TextLabel,
    collegeCourt2TextLabel,
    collegeCourt3TextLabel,
    pkpTextLabel,
    ravineTextLabel
    );

//variables for the place IDs of places on campus
var robertsonPlaceId = "ChIJSZVhetZZMogRwBndLVl2MSY";
var doanePlaceId = "ChIJ36ePv35XMogRbnL-jvGX9V4";
var schultzPlaceId = "ChIJ41q7fXlXMogRJmkoWTS2hGk";
var oddFellowsPlaceId = "ChIJx8drGH9XMogRyq9feuNRnSc";
var nv1aPlaceId = "ChIJn664UX9XMogR7kmqYknjBzU";
var nv1bPlaceId = "ChIJqTNeVn9XMogRtAX5Jquw8VA";
var nv1cPlaceId = "ChIJZ9xFWX9XMogRFwx8GH5ZaXU";
var alleghenyCommonsPlaceId = "ChIJW0VFVoBXMogREnThCMzH4sQ";
var collegeCourt1PlaceId = "ChIJg6BNWnhXMogRNcZPyCWC9rg";
var collegeCourt2PlaceId = "ChIJaaP0RHhXMogRDWzEPNzxGXU";
var collegeCourt3PlaceId = "ChIJeZIeXXhXMogRx2XE-V6lvtY";
var pkpPlaceId = "ChIJERKve3hXMogRdVP2LHNK5l8";
var ravinePlaceId = "ChIJibh5X4JXMogRI2aYNG9Ie5Q";

var placeIdsArray = new Array(
    robertsonPlaceId,
    doanePlaceId,
    schultzPlaceId,
    oddFellowsPlaceId,
    nv1aPlaceId,
    nv1bPlaceId,
    nv1cPlaceId,
    alleghenyCommonsPlaceId,
    collegeCourt1PlaceId,
    collegeCourt2PlaceId,
    collegeCourt3PlaceId,
    pkpPlaceId,
    ravinePlaceId
    );
console.log(textLabelArray);
console.log(placeIdsArray);

//buildingLabelFunction(textLabelArray, placeIdsArray, buildingLabelSetter);

    function buildingLabelFunction(textLabelArray, placeIdsArray, callback) {

        for (i = 0; i < placeIdsArray.length; i++) {
            currentId = placeIdsArray[i];
            var labelPos = [];
            var labelContent = [];
            geocoder.geocode({ 'placeId': currentId }, function (results, status) {
                if (status !== 'OK') {
                    window.alert('Geocoder failed due to: ' + status);
                    return;
                }

                labelPos.push(results[0].geometry.location);
                labelContent.push(results[0].address_components[0].long_name);

                if (typeof callback == 'function')
                {
                    callback(labelPos, labelContent);
                }

                //textLabelArray[i].setPosition(results[0].geometry.location);

                //textLabelArray[i].setContent(results[0].address_components[0].long_name);

                //console.log(textLabelArray);
                //textLabelArray[i].setMap(map);
            });
        }
    }

   function buildingLabelSetter(a1, a2) {
       for (i=0; i < a1.length; i++)
       {
           textLabelArray[i].setPosition(a1[i]);
           textLabelArray[i].setContent(a2[i]);
           textLabelArray[i].setMap(map);
       }
   }

//Overpass QL section


}//closes the whole map function

// allows the map to load up when page is loaded
google.maps.event.addDomListener(window, "load", initialize);
